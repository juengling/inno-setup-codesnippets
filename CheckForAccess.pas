[Code]
function InitializeSetup: Boolean;
begin
  // allow the setup to continue initially
  Result := True;
  if not RegKeyExists(HKCR, '.accdb') then
  begin
    // return False to prevent installation to continue
    Result := False;
    // and display a message box
    MsgBox('Bitte installieren Sie zuerst MS Access!', mbError, MB_OK);
  end;
end;
