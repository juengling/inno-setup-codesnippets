[Components]
Name: MDAC; Description: Microsoft Data Access Components (MDAC); Types: full custom; Check: InstallMDAC; ExtraDiskSpaceRequired: 6100000; Flags: fixed; MinVersion: 0,4.00.1381sp5

[Files]
; MDAC
Source: mdac_typ.exe; DestDir: {tmp}; Flags: deleteafterinstall; Components: MDAC

[Run]
; Install MDAC if necessary
Filename: {tmp}\mdac_typ.exe; Parameters: "/Q /C:""dasetup /QNT"""; WorkingDir: {tmp}; Flags: skipifdoesntexist; Description: MDAC-Bibliothek installieren; StatusMsg: Ich installiere die MDAC-Bibliothek. Dies kann einige Minuten dauern ...; Components: MDAC
