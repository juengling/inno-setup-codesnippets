(**
*	Checks if Visio 11 (2003) is installed
*)
Function IsVisioInstalled(): Boolean;
begin
	Result := RegValueExists(HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\Office\11.0\Visio', 'CurrentlyRegisteredVersion');
end;

(**
*	Checks if Acrobat Reader is installed
*)
Function IsAcrobatReaderInstalled(): Boolean;
begin
	Result := RegKeyExists(HKEY_CLASSES_ROOT, '.pdf\OpenWithList');
end;


(**
*	Shows message boxes at setup start, if Visio or Acrobat Reader is not installed.
*)
Function InitializeSetup(): Boolean;
begin
	If IsVisioInstalled() then
		Result := true
	else
		Result := MsgBox(ExpandConstant('{cm:VisioMissing}'), mbConfirmation, MB_YESNO)=IDYES;

	if not IsAcrobatReaderInstalled() then
		MsgBox (ExpandConstant('{cm:AcrobatReaderMissing}'), mbInformation, MB_OK);
end;
