; ***************************************************************************************************
; Setup
;
; Applikation: Beispiel-Setup f�r die Installation einer Access-Anwendung (MDE)
;              f�r mehrere Access-Versionen
; Erstellt:    Christoph J�ngling
; ***************************************************************************************************

; Betatest oder nicht
;#define BETA

; Verbindung zu Testserver oder nicht
;#define TESTSERVER


; ===================================================================================================
; Programmoptionen
; ===================================================================================================

#define AppID "Kurzbezeichnung der Applikation"
#define AppLongName "Langbezeichnung der Applikation"
#define AppVersion "1.23"
#define AppVersionExt "a"
#define AppVersionExtNum "1"
#define ODBCConnection "ODBC-Verbindungsname"
#define ServerName "ServerName"


; ===================================================================================================
; Setup-Script
; ===================================================================================================

[Languages]
#ifdef BETA
Name: de; MessagesFile: compiler:German-2-4.1.8.isl; InfoBeforeFile: Beta-InfoBefore-de.rtf
#else
Name: de; MessagesFile: compiler:German-2-4.1.8.isl; InfoBeforeFile: ReadmeBefore.rtf
#endif

[Setup]
#ifdef BETA

AppID={#AppID}�
AppName={#AppLongName} - Betaversion
AppVersion={#AppVersion}{#AppVersionExt}�
AppVerName={#AppID} {#AppVersion}{#AppVersionExt}�
VersionInfoVersion={#AppVersion}{#AppVersionExtNum}
VersionInfoTextVersion={#AppID} {#AppVersion}{#AppVersionExt}�
OutputBaseFilename={#AppID}-Setup-Beta
;AppUpdatesURL= --- nicht verwendet ---
DefaultDirName={pf}\{#AppLongName} - Betaversion
DefaultGroupName={#AppLongName} - Betaversion
BackColor=clMaroon
BackColor2=$682800

#else

AppID={#AppID}
AppName={#AppLongName}
AppVersion={#AppVersion}{#AppVersionExt}
AppVerName={#AppID} {#AppVersion}{#AppVersionExt}
VersionInfoVersion={#AppVersion}{#AppVersionExtNum}
VersionInfoTextVersion={#AppID} {#AppVersion}{#AppVersionExt}
OutputBaseFilename={#AppID}-Setup
AppUpdatesURL=file:\\server\path\{#AppID}-setup.exe
DefaultDirName={pf}\{#AppLongName}
DefaultGroupName={#AppLongName}
BackColor=clBlue
BackColor2=$682800

#endif

AppMutex={#AppLongName}

; Uncomment and configure these entries if necessary

;VersionInfoCompany=
;VersionInfoDescription=
;AppPublisher=
;AppPublisherURL=
;AppSupportURL=
;AppCopyright=

; Operating System's min and max version to install
;MinVersion=
;OnlyBelowVersion=

; CI images
;WizardSmallImageFile=
;WizardImageFile=
;UninstallDisplayIcon=myicon.ico


AllowNoIcons=false
OutputDir=.
DisableStartupPrompt=true
DisableReadyMemo=false
DisableReadyPage=false
Compression=lzma/ultra
SolidCompression=true
SourceDir=.
AlwaysShowDirOnReadyPage=false
AlwaysShowGroupOnReadyPage=false
UsePreviousSetupType=false
UsePreviousTasks=true
WindowResizable=false
WindowVisible=true
AlwaysShowComponentsList=true
DisableProgramGroupPage=true
FlatComponentsList=false
WindowStartMaximized=true
DisableFinishedPage=false
ShowTasksTreeLines=true
UsePreviousUserInfo=false
UninstallFilesDir={app}\uninstall
ShowLanguageDialog=no

[Components]
Name: MDAC; Description: Microsoft Data Access Components (MDAC); Types: full custom; Check: InstallMDAC; ExtraDiskSpaceRequired: 6100000; Flags: fixed; MinVersion: 0,4.00.1381sp5
Name: A97; Description: Version f�r Access 97; Types: full custom; Check: AccVerExists(8)
Name: A2000; Description: Version f�r Access 2000; Types: full custom; Check: AccVerExists(9)
Name: A2002; Description: Version f�r Access 2002; Types: full custom; Check: AccVerExists(10)
Name: Dokumentation; Description: Dokumentation; Types: custom full
Name: Hilfsdateien; Description: Zus�tzliche Dateien; Types: custom full

[Types]
Name: full; Description: Vollst�ndig
Name: custom; Description: Benutzerdefiniert; Flags: iscustom

[Tasks]
#ifdef TESTSERVER

Name: ODBC-NT; Description: &ODBC-Datenquelle f�r Testserver erzeugen oder aktualisieren; GroupDescription: Datenzugriff:; Components: A97 A2000 A2002; OnlyBelowVersion: 0,5
Name: ODBC-2000-XP; Description: &ODBC-Datenquelle f�r Testserver erzeugen oder aktualisieren; GroupDescription: Datenzugriff:; Components: A97 A2000 A2002; MinVersion: 0,5; Check: AdminLoggedOn

#else

Name: ODBC-NT; Description: &ODBC-Datenquelle erzeugen oder aktualisieren; GroupDescription: Datenzugriff:; Components: A97 A2000 A2002; OnlyBelowVersion: 0,5
Name: ODBC-2000-XP; Description: &ODBC-Datenquelle erzeugen oder aktualisieren; GroupDescription: Datenzugriff:; Components: A97 A2000 A2002; MinVersion: 0,5; Check: AdminLoggedOn

#endif

Name: desktopicon; Description: &Desktop Icon erzeugen; GroupDescription: Icons:
Name: quicklaunchicon; Description: &Quick Launch Icon erzeugen; Flags: unchecked; GroupDescription: Icons:

[Files]
; MDAC
Source: mdac_typ.exe; DestDir: {tmp}; Flags: deleteafterinstall external; Components: MDAC

; Files for several Access versions
Source: MyApplication-97.mde; DestDir: {app}; Flags: ignoreversion; Components: A97; DestName: MyApplication.mde
Source: MyApplication-2k.mde; DestDir: {app}; Flags: ignoreversion; Components: A2000; DestName: MyApplication.mde

; F�r AX wird ebenfalls die 2000er MDE verwendet
Source: MyApplication-2k.mde; DestDir: {app}; Flags: ignoreversion; Components: A2002; DestName: MyApplication.mde

; INI file
Source: MyApplication.ini; DestDir: {app}; Components: A97 A2000 A2002; Flags: onlyifdoesntexist

; Other files
Source: MyApplication.ico; DestDir: {app}; Flags: promptifolder; Components: A97 A2000 A2002
Source: MyApplication.bmp; DestDir: {app}; Flags: promptifolder; Components: A97 A2000 A2002
Source: MyApplication.jpg; DestDir: {app}; Flags: promptifolder; Components: Dokumentation
Source: Versionshistorie.htm; DestDir: {app}; Flags: ignoreversion; Components: Dokumentation
Source: Handbuch.pdf; DestDir: {app}; Components: Dokumentation
Source: ResetFormPositions.reg; DestDir: {app}; Flags: ignoreversion; Components: Hilfsdateien

[Icons]
; Icons for several Access versions - Start Menu
Name: {group{#AppLongName}; Filename: {code:GetAccessPath|97}; Parameters: """{app}\MyApplication.mde"""; WorkingDir: {app}; IconFilename: {app}\MyApplication.ico; IconIndex: 0; Components: A97
Name: {group}\{#AppLongName}; Filename: {code:GetAccessPath|2000}; Parameters: """{app}\MyApplication.mde"""; WorkingDir: {app}; IconFilename: {app}\MyApplication.ico; IconIndex: 0; Components: A2000
Name: {group}\{#AppLongName}; Filename: {code:GetAccessPath|2002}; Parameters: """{app}\MyApplication.mde"""; WorkingDir: {app}; IconFilename: {app}\MyApplication.ico; IconIndex: 0; Components: A2002

; Icons for several Access versions - Repair & Compact - Start Menu
Name: {group}\Reparieren und Komprimieren; Filename: {code:GetAccessPath|97}; Parameters: """{app}\MyApplication.mde"" /repair /compact"; WorkingDir: {app}; Comment: Reparieren und Komprimieren; Components: A97
Name: {group}\Reparieren und Komprimieren; Filename: {code:GetAccessPath|2000}; Parameters: """{app}\MyApplication.mde"" /repair /compact"; WorkingDir: {app}; Comment: Reparieren und Komprimieren; Components: A2000
Name: {group}\Reparieren und Komprimieren; Filename: {code:GetAccessPath|2002}; Parameters: """{app}\MyApplication.mde"" /repair /compact"; WorkingDir: {app}; Comment: Reparieren und Komprimieren; Components: A2002

; Andere Dateien
Name: {group}\Handbuch; Filename: {app}\Handbuch.pdf; IconIndex: 0; WorkingDir: {app}; Flags: createonlyiffileexists; Components: Dokumentation
Name: {group}\Versionshistorie; Filename: {app}\Versionshistorie.htm; IconIndex: 0; WorkingDir: {app}; Comment: Zeige Versionshistorie; Flags: createonlyiffileexists; Components: Dokumentation

#ifdef BETA

; Icons for several Access versions - Desktop
Name: {commondesktop}\{#AppLongName} - Beta; Filename: {code:GetAccessPath|97}; Parameters: """{app}\MyApplication.mde"""; Tasks: desktopicon; WorkingDir: {app}; IconFilename: {app}\MyApplication.ico; IconIndex: 0; Components: A97
Name: {commondesktop}\{#AppLongName} - Beta; Filename: {code:GetAccessPath|2000}; Parameters: """{app}\MyApplication.mde"""; Tasks: desktopicon; WorkingDir: {app}; IconFilename: {app}\MyApplication.ico; IconIndex: 0; Components: A2000
Name: {commondesktop}\{#AppLongName} - Beta; Filename: {code:GetAccessPath|2002}; Parameters: """{app}\MyApplication.mde"""; Tasks: desktopicon; WorkingDir: {app}; IconFilename: {app}\MyApplication.ico; IconIndex: 0; Components: A2002

; Icons for several Access versions - Quick Launch Bar
Name: {commonappdata}\Microsoft\Internet Explorer\Quick Launch\{#AppLongName} - Beta; Filename: {code:GetAccessPath|97}; Parameters: """{app}\MyApplication.mde"""; Tasks: quicklaunchicon; WorkingDir: {app}; IconFilename: {app}\MyApplication.ico; IconIndex: 0; Components: A97
Name: {commonappdata}\Microsoft\Internet Explorer\Quick Launch\{#AppLongName} - Beta; Filename: {code:GetAccessPath|2000}; Parameters: """{app}\MyApplication.mde"""; Tasks: quicklaunchicon; WorkingDir: {app}; IconFilename: {app}\MyApplication.ico; IconIndex: 0; Components: A2000
Name: {commonappdata}\Microsoft\Internet Explorer\Quick Launch\{#AppLongName} - Beta; Filename: {code:GetAccessPath|2002}; Parameters: """{app}\MyApplication.mde"""; Tasks: quicklaunchicon; WorkingDir: {app}; IconFilename: {app}\MyApplication.ico; IconIndex: 0; Components: A2002

#else

; Icons for several Access versions - Desktop
Name: {commondesktop}\{#AppLongName}; Filename: {code:GetAccessPath|97}; Parameters: """{app}\MyApplication.mde"""; Tasks: desktopicon; WorkingDir: {app}; IconFilename: {app}\MyApplication.ico; IconIndex: 0; Components: A97
Name: {commondesktop}\{#AppLongName}; Filename: {code:GetAccessPath|2000}; Parameters: """{app}\MyApplication.mde"""; Tasks: desktopicon; WorkingDir: {app}; IconFilename: {app}\MyApplication.ico; IconIndex: 0; Components: A2000
Name: {commondesktop}\{#AppLongName}; Filename: {code:GetAccessPath|2002}; Parameters: """{app}\MyApplication.mde"""; Tasks: desktopicon; WorkingDir: {app}; IconFilename: {app}\MyApplication.ico; IconIndex: 0; Components: A2002

; Icons for several Access versions - Quick Launch Bar
Name: {commonappdata}\Microsoft\Internet Explorer\Quick Launch\{#AppLongName}; Filename: {code:GetAccessPath|97}; Parameters: """{app}\MyApplication.mde"""; Tasks: quicklaunchicon; WorkingDir: {app}; IconFilename: {app}\MyApplication.ico; IconIndex: 0; Components: A97
Name: {commonappdata}\Microsoft\Internet Explorer\Quick Launch\{#AppLongName}; Filename: {code:GetAccessPath|2000}; Parameters: """{app}\MyApplication.mde"""; Tasks: quicklaunchicon; WorkingDir: {app}; IconFilename: {app}\MyApplication.ico; IconIndex: 0; Components: A2000
Name: {commonappdata}\Microsoft\Internet Explorer\Quick Launch\{#AppLongName}; Filename: {code:GetAccessPath|2002}; Parameters: """{app}\MyApplication.mde"""; Tasks: quicklaunchicon; WorkingDir: {app}; IconFilename: {app}\MyApplication.ico; IconIndex: 0; Components: A2002

; Icons for all Versions (only non-BETA)
Name: {group}\Update vom Netz installieren; Filename: \\n49nacnhome\clearing_01$\nics-setup.exe; WorkingDir: {app}
Name: {group}\Formularpositionen zur�cksetzen; Filename: {app}\ResetFormPositions.reg; WorkingDir: {app}; Comment: Zur�cksetzen der Formularpositionen; IconIndex: 0; Flags: createonlyiffileexists; Components: Hilfsdateien

#endif

[INI]
#ifdef BETA

; Automatisch den Dateinamen der Setup.exe schreiben
Filename: {app}\NICS.ini; Section: Setup; Key: FileName; String: {code:GetUNCFilename|{srcexe}}

#else

; Einen festen Dateinamen schreiben
Filename: {app}\NICS.ini; Section: Setup; Key: FileName; String: \\n49nacnhome\clearing_01\nics-setup.exe

#endif

; Alte Fehlermeldungsunterdr�ckung wieder aufheben
Filename: {app}\NICS.ini; Section: Setup; Key: NoErrors; String: 0


[Registry]
#ifdef TESTSERVER

; ODBC-Verbindung anlegen
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\ODBC Data Sources; ValueType: string; ValueName: {#ODBCConnection}_Test; ValueData: SQL Server; Flags: uninsdeletevalue; Tasks: ODBC-NT ODBC-2000-XP

Root: HKLM; SubKey: Software\ODBC\ODBC.INI\{#ODBCConnection}_Test; ValueName: Driver; ValueType: string; ValueData: "{reg:HKLM\Software\ODBC\ODBCINST.INI\SQL Server,Driver|SQLSRV32.dll} "; Tasks: ODBC-NT ODBC-2000-XP
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\{#ODBCConnection}_Test; ValueName: Description; ValueType: string; ValueData: NICS-Datenbank; Tasks: ODBC-NT ODBC-2000-XP
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\{#ODBCConnection}_Test; ValueName: Server; ValueType: string; ValueData: N49NHHTSQL01; Tasks: ODBC-NT ODBC-2000-XP
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\{#ODBCConnection}_Test; ValueName: Database; ValueType: string; ValueData: HHT32_D_ICL; Tasks: ODBC-NT ODBC-2000-XP
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\{#ODBCConnection}_Test; ValueName: Language; ValueType: string; ValueData: Deutsch; Tasks: ODBC-NT ODBC-2000-XP
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\{#ODBCConnection}_Test; ValueName: Trusted_Connection; ValueType: string; ValueData: yes; Tasks: ODBC-NT ODBC-2000-XP

; ODBC-Verbindung beim Deinstallieren l�schen
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\{#ODBCConnection}_Test; Flags: uninsdeletekey dontcreatekey

; Programmoptionen beim Deinstallieren l�schen
Root: HKCU; SubKey: Software\VB and VBA Program Settings\{#AppLongName} - Beta; Flags: uninsdeletekey

#else

; ODBC-Verbindung anlegen
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\ODBC Data Sources; ValueType: string; ValueName: {#ODBCConnection}; ValueData: SQL Server; Flags: uninsdeletevalue; Tasks: ODBC-NT ODBC-2000-XP

Root: HKLM; SubKey: Software\ODBC\ODBC.INI\{#ODBCConnection}; ValueName: Driver; ValueType: string; ValueData: "{reg:HKLM\Software\ODBC\ODBCINST.INI\SQL Server,Driver|SQLSRV32.dll} "; Tasks: ODBC-NT ODBC-2000-XP
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\{#ODBCConnection}; ValueName: Description; ValueType: string; ValueData: NICS-Datenbank; Tasks: ODBC-NT ODBC-2000-XP
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\{#ODBCConnection}; ValueName: Server; ValueType: string; ValueData: {#ServerName}; Tasks: ODBC-NT ODBC-2000-XP
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\{#ODBCConnection}; ValueName: Database; ValueType: string; ValueData: HHT32_D_ICL; Tasks: ODBC-NT ODBC-2000-XP
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\{#ODBCConnection}; ValueName: Language; ValueType: string; ValueData: Deutsch; Tasks: ODBC-NT ODBC-2000-XP
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\{#ODBCConnection}; ValueName: Trusted_Connection; ValueType: string; ValueData: yes; Tasks: ODBC-NT ODBC-2000-XP

; ODBC-Verbindung beim Deinstallieren l�schen
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\{#ODBCConnection}; Flags: uninsdeletekey dontcreatekey

; Programmoptionen beim Deinstallieren l�schen
Root: HKCU; SubKey: Software\VB and VBA Program Settings\{#AppLongName}; Flags: uninsdeletekey

; Alte ODBC-Verbindungen generell l�schen
; ICL
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\ODBC Data Sources; ValueName: ICL; Flags: deletevalue
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\ICL; Flags: deletekey

; ICL-Test
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\ODBC Data Sources; ValueName: ICL-Test; Flags: deletevalue
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\ICL-Test; Flags: deletekey

#endif

; Create program option entries
Root: HKCU; Subkey: Software\VB and VBA Program Settings\{#AppLongName}\ShowBarcodes\; ValueType: string; ValueName: Zeiten; ValueData: 0; Flags: dontcreatekey
Root: HKCU; Subkey: Software\VB and VBA Program Settings\{#AppLongName}\ShowCoding\; ValueType: string; ValueName: Zeiten; ValueData: 0; Flags: dontcreatekey
Root: HKCU; SubKey: Software\VB and VBA Program Settings\{#AppLongName}\Optionen; ValueType: string; ValueName: SetupFehler; ValueData: 0; Flags: dontcreatekey

; L�schen, was in neuer Version nicht mehr ben�tigt wird
Root: HKCU; Subkey: Software\VB and VBA Program Settings\{#AppLongName}\ShowBarcodes\; ValueName: EAN - uncleared; Flags: deletekey
Root: HKCU; Subkey: Software\VB and VBA Program Settings\{#AppLongName}\ShowBarcodes\; ValueName: PZN - uncleared; Flags: deletekey

Root: HKCU; Subkey: Software\VB and VBA Program Settings\{#AppLongName}\ShowBarcodes\; ValueName: EAN - intermediate; Flags: deletekey
Root: HKCU; Subkey: Software\VB and VBA Program Settings\{#AppLongName}\ShowBarcodes\; ValueName: PZN - intermediate; Flags: deletekey

Root: HKCU; Subkey: Software\VB and VBA Program Settings\{#AppLongName}\ShowBarcodes\; ValueName: EAN - active; Flags: deletekey
Root: HKCU; Subkey: Software\VB and VBA Program Settings\{#AppLongName}\ShowBarcodes\; ValueName: PZN - active; Flags: deletekey

[InstallDelete]
; Altes Handbuch l�schen
Name: {app}\Handbuch.doc; Type: files

; Falls ein Benutzer das Setup einmal ohne Adminrechte ausgef�hrt hat,
; werden im Endeffekt zwei Icons angelegt: F�r "All Users" und f�r "User".
; Daher werden hier alle benutzerspezifische Icons auf dem Desktop gel�scht,
; falls Adminrechte vorhanden sind.

; Desktop
Name: {userdesktop}\{#AppLongName}; Type: filesandordirs; Check: AdminLoggedOn

; Startmen�: Gesamte Gruppe l�schen
Name: {userstartmenu}\{groupname}; Type: filesandordirs; Check: AdminLoggedOn


[UninstallDelete]
Name: {app}; Type: filesandordirs

[Run]
; Install MDAC if necessary
Filename: {tmp}\mdac_typ.exe; Parameters: "/Q /C:""dasetup /QNT"""; WorkingDir: {tmp}; Flags: skipifdoesntexist; Description: MDAC-Bibliothek installieren; StatusMsg: Ich installiere die MDAC-Bibliothek. Dies kann einige Minuten dauern ...; Components: MDAC

; Re-run Application if "silent running"
Filename: {code:GetAccessPath|97}; Parameters: """{app}\MyApplication.mde"" /cmd /updatefinished"; WorkingDir: {app}; Flags: nowait skipifdoesntexist postinstall skipifnotsilent; Components: A97
Filename: {code:GetAccessPath|2000}; Parameters: """{app}\MyApplication.mde"" /cmd /updatefinished"; WorkingDir: {app}; Flags: nowait skipifdoesntexist postinstall skipifnotsilent; Components: A2000
Filename: {code:GetAccessPath|2002}; Parameters: """{app}\MyApplication.mde"" /cmd /updatefinished"; WorkingDir: {app}; Flags: nowait skipifdoesntexist postinstall skipifnotsilent; Components: A2002

[Messages]
WelcomeLabel2=Er wird jetzt [name/ver] auf Ihrem Computer installieren.%n%nKurzbeschreibung: ...%n%nSie sollten alle anderen Anwendungen beenden, bevor Sie mit dem Setup fortfahren. Dadurch werden eventuelle Konflikte w�hrend der Installation vermieden.

#ifdef BETA
BeveledLabel=Betatest - Development Department
#else
BeveledLabel=Development Department
#endif

[_ISTool]
OutputExeFilename=.

[Code]
// Die Dokumentation erfolgt analog zu PHPDoc (siehe www.phpdoc.de) bzw. JavaDoc (siehe

(**
* Liefert die vereinheitlichte Access-Versionsnummer als String
* z.B. f�r die Weiterverwendung in der Registry
*
* @param	string Access-Versionsnummer (8, 97, 9, 2000, 10, 2002, ...)
* @return	string Access-Versionsnummer (8, 9, 10, ...)
* @author	Christoph J�ngling <Christoph@Juengling-EDV.de>
* @link		http://www.Juengling-EDV.de
* @version  2003-02-04
*)
Function GetAccessVersionNumber(Version: string): string;
var
	n: LongInt;
begin
  n := StrToInt(Version);
  case n of
		8:    Result := '8';
		97:   Result := '8';

		9:    Result := '9';
		2000: Result := '9';

		10:   Result := '10';
		2002: Result := '10';

		else Result := '';
	end;
end;


(**
* Liefert den Pfad zur Access-EXE-Datei mit der angegebenen Versionsnummer
*
* Ist die gew�nschte Version nicht installiert,
* liefert die Funktion einen leeren String
*
* @return	string	Pfad zur EXE-Datei oder Leerstring
* @param	string	Versionsnummer
* @author	Christoph J�ngling <Christoph@Juengling-EDV.de>
* @link		http://www.Juengling-EDV.de
* @version  2003-02-04
*)
Function GetAccessPath(Default: String): String;
var
	SubKeyName, Classid, Path: string;
	ok: Boolean;
begin
	Result := '';
	SubKeyName := 'Access.Application.' + GetAccessVersionNumber(Default);
	ok := RegQueryStringValue(HKCR, SubKeyName + '\CLSID', '', Classid);
	if ok then ok := RegQueryStringValue(HKCR, 'CLSID\' + Classid + '\LocalServer32', '', Path);
	if ok then Result := Path;
end;


(**
* Pr�fe, ob die angegebene Access-Version auf dem Rechner installiert ist
* Es wird nur TRUE zur�ckgegeben, falls die gepr�fte Version auch die
* h�chste installierte ist.
*
* @return	boolean	TRUE = Version ist installiert, FALSE = Version ist nicht installiert
* @author	Christoph J�ngling <Christoph@Juengling-EDV.de>
* @link		http://www.Juengling-EDV.de
* @version	2003-01-13
*)
Function AccVerExists(Version: string): Boolean;

var
	A8, A9, A10: boolean;

begin
	Result := false;

	A8  := (GetAccessPath('8') <> '');
	A9  := (GetAccessPath('9') <> '');
	A10 := (GetAccessPath('10') <> '');

	case Version of
    	'8':  Result := A8 and not A9 and not A10;
    	'9':  Result := A9 and not A10;
    	'10': Result := A10;
	end;
end;


procedure DecodeVersion(verstr: String; var verint: array of Integer);
var
  i,p: Integer; s: string;
begin
  // initialize array
  verint := [0,0,0,0];
  i := 0;
  while ( (Length(verstr) > 0) and (i < 4) ) do
  begin
  	p := pos('.', verstr);
  	if p > 0 then
  	begin
      if p = 1 then s:= '0' else s:= Copy( verstr, 1, p - 1 );
  	  verint[i] := StrToInt(s);
  	  i := i + 1;
  	  verstr := Copy( verstr, p+1, Length(verstr));
  	end
  	else
  	begin
  	  verint[i] := StrToInt( verstr );
  	  verstr := '';
  	end;
  end;

end;

function GetMDACVersion(): String;
var
  sVersion:  String;
  ok:        Boolean;
begin
  sVersion := '';

  // Method 1
  //ok := GetVersionNumbersString(ExpandConstant('{cf}\System\Ado\msado15.dll'), sVersion);

  // Method 2
  ok := RegQueryStringValue(HKLM, 'Software\Microsoft\DataAccess\', 'Version', sVersion);

  if ok then Result := sVersion;

  // Test
  //MsgBox('MDAC-Version: ' + Result, mbError, MB_OK);
end;


// This function compares version string
// return -1 if ver1 < ver2
// return  0 if ver1 = ver2
// return  1 if ver1 > ver2
function CompareVersion(ver1, ver2: String): Integer;
var
  verint1, verint2: array of Integer;
  i: integer;
begin
	if ver1 = ''
	then Result := -2
	else
		if ver2 = ''
		then Result := 2
		else begin
			SetArrayLength(verint1, 4);
			DecodeVersion(ver1, verint1);

			SetArrayLength(verint2, 4);
			DecodeVersion(ver2, verint2);

			Result := 0; i := 0;
			while ((Result = 0) and (i < 4)) do begin
				if verint1[i] > verint2[i]
				then Result := 1
				else
					if verint1[i] < verint2[i]
					then Result := -1
					else Result := 0;
				i := i + 1;
			end;
		end;
end;


(**
* Pr�fe, ob die MDAC-Version ausreichend ist oder installiert werden muss
*
* @return	boolean	TRUE = MDAC muss installiert werden, FALSE = Installation nicht notwendig
* @author	Christoph J�ngling <Christoph@Juengling-EDV.de>
* @link		http://www.Juengling-EDV.de
* @version	2003-01-08
*)
Function InstallMDAC(): Boolean;
begin
	Result := (CompareVersion(GetMDACVersion(), '2.50') < 0)
end;


(**
* Diese Funktion dient nur zur Weitergabe der Information an einen Check-Parameter
*
* @return	boolean	TRUE = Administrator angemeldet
* @author	Christoph J�ngling <Christoph@Juengling-EDV.de>
* @link		http://www.Juengling-EDV.de
* @version  2003-02-04
*)
Function AdminLoggedOn: Boolean;
begin
	Result := IsAdminLoggedOn();
end;


(**
* Diese Funktion liefert den UNC-Namen der aktuell laufenden Setup-Datei
*
* @return	string UNC-Name der aktuell laufenden Setup-Datei
* @author	Christoph J�ngling <Christoph@Juengling-EDV.de>
* @link		http://www.Juengling-EDV.de
* @version  2003-04-02
*)
Function GetUNCFilename(Default: String): String;
begin
	Result := ExpandUNCFileName(ExpandConstant(Default));
end;


(**
* Pr�fe, ob die ODBC-Verbindung eingerichtet werden mu�.
*
* @return	boolean	TRUE = ODBC-Verbindung mu� eingerichtet werden
* @author	Christoph J�ngling <Christoph@Juengling-EDV.de>
* @link		http://www.Juengling-EDV.de
* @version	2003-04-15
*)
Function OdbcConnectionToBeSet(): Boolean;
var sDriverName: string;
begin
  Result := not RegQueryStringValue(HKLM, 'SOFTWARE\ODBC\ODBC.INI\ODBC Data Sources\', 'ICL', sDriverName);
end;



(**
* Pr�fe, ob eine der unterst�tzten Access- und MDAC-Versionen auf dem Rechner installiert ist
*
* @return	boolean	TRUE = alles notwendige ist installiert, FALSE = es fehlt was
* @author	Christoph J�ngling <Christoph@Juengling-EDV.de>
* @link		http://www.Juengling-EDV.de
* @version	2002-12-20
*)
Function InitializeSetup(): Boolean;
var
	ok, Win2k: boolean;
	Meldung: string;
begin

// Positiv denken!
Result := true;

// Erfolgt die Installation unter Windows 2000 oder XP?
Win2k := (InstallOnThisVersion('0, 5.0.2195', '0, 0') = irInstall);

(*************************************************************************
	Zu Testzwecken Meldung ausgeben
*************************************************************************

Meldung := 'Windows: ';
if Win2k then Meldung := Meldung + '2000 oder h�her' else Meldung := Meldung + 'NT 4';

Meldung := Meldung + chr(13) + chr(10) + 'Access-Version: ';
if AccVerExists('8') then Meldung := Meldung + '97';
if AccVerExists('9') then Meldung := Meldung + '2000';
if AccVerExists('10') then Meldung := Meldung + 'XP';

Meldung := Meldung + chr(13) + chr(10) + 'Admin eingeloggt: ';
if IsAdminLoggedOn() then Meldung := Meldung + 'Ja' else Meldung := Meldung + 'Nein';

Meldung := Meldung + chr(13) + chr(10) + 'ODBC-Verbindung anlegen: ';
if OdbcConnectionToBeSet() then Meldung := Meldung + 'Ja' else Meldung := Meldung + 'Nein';

MsgBox(Meldung, mbInformation, MB_OK);

*************************************************************************
	Zu Testzwecken Meldung ausgeben
*************************************************************************)

// Pr�fe installierte Access-Versionen
ok := AccVerExists('8') or AccVerExists('9') or AccVerExists('10');
//ok := AccVerExists('8') or AccVerExists('9');
if not ok then begin
  MsgBox('Das Setup kann nicht ausgef�hrt werden, da keine g�ltige Access-Version (97, 2000 oder XP) gefunden wurde.', mbError, MB_OK);
  //MsgBox('Das Setup kann nicht ausgef�hrt werden, da keine g�ltige Access-Version (97 oder 2000) gefunden wurde.', mbError, MB_OK);
  Result := false;
end;

// F�r die Einrichtung der ODBC-Verbindung sind ab Windows 2000 Admin-Rechte erforderlich
if OdbcConnectionToBeSet() and not IsAdminLoggedOn() and Win2k then begin
  MsgBox('F�r NICS ist eine ODBC-Verbindung erforderlich. Um diese einzurichten, sind wiederum Administrator-Rechte erforderlich.', mbError, MB_OK);
  Result := false;
end;

end;
