; Install an Excel template called "Excel-Template.xltx" into that
; personal template folder the user has choosen to use.
;
; The function GetExcelVersionNumberAsString() only gets the version
; number of the highest installed Excel version. This is then used
; to create the Registry path with the user's setting of the template
; path.
;
; The installation defaults to the {app} folder, if no path could begin
; determined.
;
; See https://gitlab.com/juengling/inno-setup-codesnippets for more.

[Files]
Source: "Excel-Template.xltx"; DestDir: "{reg:HKCU\Software\Microsoft\Office\{code:GetExcelVersionNumberAsString}\Excel\Options,PersonalTemplates|{app}}"

[Code]
function GetExcelVersionNumberAsString(dummy: String): String;
(*
    Search Registry for installed Excel version. From highests version down.
    The parameter "dummy" is not used, but InnoSetup forces me to define one.

    Basic code came from https://dutchgemini.wordpress.com/category/innosetup-2/
	and has been expanded by me to cover some newer versions.
*)
begin
    if RegValueExists(HKEY_LOCAL_MACHINE, 'Software\Microsoft\Office\16.0\Excel\InstallRoot', 'Path') then begin
        Result := '16.0';
    end else if RegValueExists(HKEY_LOCAL_MACHINE, 'Software\Microsoft\Office\15.0\Excel\InstallRoot', 'Path') then begin
        Result := '15.0';
    end else if RegValueExists(HKEY_LOCAL_MACHINE, 'Software\Microsoft\Office\14.0\Excel\InstallRoot', 'Path') then begin
        Result := '14.0';
    end else if RegValueExists(HKEY_LOCAL_MACHINE, 'Software\Microsoft\Office\12.0\Excel\InstallRoot', 'Path') then begin
        Result := '12.0';
    end else if RegValueExists(HKEY_LOCAL_MACHINE, 'Software\Microsoft\Office\11.0\Excel\InstallRoot', 'Path') then begin
        Result := '11.0';
    end else if RegValueExists(HKEY_LOCAL_MACHINE, 'Software\Microsoft\Office\10.0\Excel\InstallRoot', 'Path') then begin
        Result := '10.0';
    end else if RegValueExists(HKEY_LOCAL_MACHINE, 'Software\Microsoft\Office\9.0\Excel\InstallRoot', 'Path') then begin
        Result := '9.0';
    end else begin
        Result := '0.0';
    end;
end;

