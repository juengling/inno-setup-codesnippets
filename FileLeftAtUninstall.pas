(**
	Check if any file was left in APP directory after uninstallation
*)
procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
var
  FindRec: TFindRec;

begin

if CurUninstallStep = usDone then begin
	if FindFirst(ExpandConstant('{app}') + '\*.*', FindRec) then
		MsgBox(ExpandConstant(ExpandConstant('{cm:RemainingFiles}')), mbInformation, MB_OK);
	FindClose(FindRec);
end;

end;