(**
*	Creates a version number for the display text
*)
Function ReadableVersionNumber(VersionNumber: String): String;

var
	dot: integer;
	sngVersion: double;
	strVersion: string;

begin

Result := '';

// 61.3.0.123
dot := pos('.', VersionNumber);
if dot > 0 then begin
	strVersion := copy(VersionNumber, 1, dot-1);
	VersionNumber := copy(VersionNumber, dot+1, 2);
	sngVersion := StrToFloat(strVersion);
	sngVersion := sngVersion / 10;
	strVersion := FloatToStr(sngVersion);
	StringChange(strVersion, ',', '.');
	dot := pos('.',strVersion);
	if dot = 0 then strVersion := strVersion + '.0';
	Result := strVersion;

	// 3.0.123
	dot := pos('.', VersionNumber);
	if dot > 0 then begin
		strVersion := copy(VersionNumber, 1, dot-1);
		Result := Result + '.' + strVersion;
end else
		Result := Result + '.0';
end;
end;
