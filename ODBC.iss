[Registry]
; Create ODBC connection
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\ODBC Data Sources; ValueType: string; ValueName: XXX; ValueData: SQL Server; Flags: uninsdeletevalue

; Read the complete path to the SQL Server driver DLL from the registry
; Default = filename only
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\XXX; ValueName: Driver; ValueType: string; ValueData: {reg:HKLM\Software\ODBC\ODBCINST.INI\SQL Server,Driver|SQLSRV32.dll}

Root: HKLM; SubKey: Software\ODBC\ODBC.INI\XXX; ValueName: Server; ValueType: string; ValueData: SERVERNAME
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\XXX; ValueName: Database; ValueType: string; ValueData: DATABASENAME
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\XXX; ValueName: Trusted_Connection; ValueType: string; ValueData: yes

; the description and language name are optional
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\XXX; ValueName: Description; ValueType: string; ValueData: XXX Description
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\XXX; ValueName: Language; ValueType: string; ValueData: LANGUAGENAME

; Delete the complete ODBC key on uninstall
Root: HKLM; SubKey: Software\ODBC\ODBC.INI\XXX; Flags: uninsdeletekey dontcreatekey
